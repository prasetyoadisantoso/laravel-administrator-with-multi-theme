<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/* Administrator Panel */


// Route to Settings Management
Route::prefix('settings')->group(function() {
    Route::get('/', 'SettingsController@index')->name('settings');
});

// Route to Cache Management
Route::prefix('cache')->group(function() {
    Route::get('/', 'CacheController@index')->name('cache');
    Route::get('config-clear', 'CacheController@configClear')->name('config-clear');
    Route::get('cache-clear', 'CacheController@cacheClear')->name('cache-clear');
    Route::get('view-clear', 'CacheController@viewClear')->name('view-clear');
    Route::get('route-clear', 'CacheController@routeClear')->name('route-clear');
});

Route::prefix('theme')->group(function() {
    Route::post('store', 'SettingsController@theme')->name('theme.store');
});
