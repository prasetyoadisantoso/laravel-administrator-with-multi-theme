<?php

namespace Modules\Settings\Entities;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// Models to get data from database of settings table
class Settings extends Model
{
    // use HasFactory;

    protected $primaryKey = null;
    public $incrementing = false;
    protected $fillable = ['themes'];


}
