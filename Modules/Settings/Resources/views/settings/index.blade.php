@extends('administrator::dashboard')
@section('setting-index')


<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Administration Settings</h1>
                <small>&nbsp; Configuration your web </small>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('settings')}}">Home</a></li>
                    <li class="breadcrumb-item active">Settings</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section>
    @if ($message = Session::get('success'))
    <div class="alert alert-success mx-5 alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p>{{ $message }}</p>
    </div>
    @endif
</section>



<!-- Main content -->
<section class="content">
    <div class="container-fluid">


        <div class="row">
            <div class="col-lg-5 col-xs-12 col-sm-12 col-md-12">
                <div class="card">


                    <div class="alert alert-danger alert-dismissible my-5 mx-5">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h3><i class="icon fas fa-ban"></i> Warning !</h3>
                        Before change theme to Clean Blog, please download <a href="https://gitlab.com/prasetyo.element/icicle-front-end-development">Clean Blog</a> and place it to resource/themes directory. Then, download and enable CMS Modules at <a href="https://gitlab.com/prasetyo.element/icicle-cms-module">ICICLE CMS Modules</a>
                    </div>

                    {{-- Form Change Theme --}}
                    <div class="card-body">
                        {!! Form::open(array('route' => 'theme.store','method'=>'POST')) !!}
                        <div class="row">
                            <div class="col-12 padding-box">
                                <div class="form-group">
                                    <strong>Theme:</strong>
                                    {!! Form::select('themes', ['default' => 'Default', 'blog' => 'Clean Blog'
                                    ],null, array('class' =>
                                    'form-control')); !!}
                                </div>
                                <div class="mt-1 text-center">
                                    <button type="submit" class="btn btn-success">Set Theme</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>



@endsection
