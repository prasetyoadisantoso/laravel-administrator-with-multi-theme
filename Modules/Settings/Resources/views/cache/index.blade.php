@extends('administrator::dashboard')
@section('cache-index')


<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Cache Management</h1>
                <small>&nbsp; Clear Cache for Refresh the Website</small>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('settings')}}">Home</a></li>
                    <li class="breadcrumb-item active">Cache</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->



<section>
    @if ($message = Session::get('success'))
    <div class="alert alert-success mx-5 alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p>{{ $message }}</p>
    </div>
    @endif
</section>




<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <div class="pull-left">
                            <h3>Manage cache</h3>
                        </div>
                    </div>


                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">

                            {{-- Responsive Table --}}
                            <div class="box-body table-responsive">
                                <table id="example1" class="example1 table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Command List</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Clear config | command:"config:clear"</td>
                                            <td>
                                                <a class="btn btn-warning" href="{{ route('config-clear') }}"> Config
                                                    Clear</a>
                                            </td>
                                        </tr>
                                    </tbody>

                                    <tbody>
                                        <tr>
                                            <td>2</td>
                                            <td>Clear cache | command:"cache:clear"</td>
                                            <td>
                                                <a class="btn btn-warning" href="{{ route('cache-clear') }}"> Cache
                                                    Clear</a>
                                            </td>
                                        </tr>
                                    </tbody>

                                    <tbody>
                                        <tr>
                                            <td>3</td>
                                            <td>Clear view | command:"view:clear"</td>
                                            <td>
                                                <a class="btn btn-warning" href="{{ route('view-clear') }}"> View
                                                    Clear</a>
                                            </td>
                                        </tr>
                                    </tbody>

                                    <tbody>
                                        <tr>
                                            <td>3</td>
                                            <td>Clear routes | command:"route:clear"</td>
                                            <td>
                                                <a class="btn btn-warning" href="{{ route('route-clear') }}"> Route
                                                    Clear</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
</section>


@endsection
