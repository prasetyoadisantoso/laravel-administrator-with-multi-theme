@extends('administrator::dashboard')
@section('user-index')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">User Management</h1>
                <small>&nbsp; List of users with its permissions</small>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('users')}}">Home</a></li>
                    <li class="breadcrumb-item active">Create User</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->


<section>
    @if ($message = Session::get('success'))
    <div class="alert alert-success mx-5 alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p>{{ $message }}</p>
    </div>
    @endif
</section>



<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <div class="pull-left">
                            <h3>List of Roles</h3>
                        </div>

                        {{-- Button Create Roles --}}
                        <div class="ml-auto">
                            @can('role-create')
                            <a class="btn btn-outline-success" href="{{route('users.create')}}">
                                <i class="fas fa-user-edit mr-2"></i> Create New User</a>
                            @endcan
                        </div>
                    </div>


                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            {{-- Responsive Table --}}

                            <div class="box-body table-responsive">
                                <table id="example1" class="example1 table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Roles</th>
                                            <th width="280px">Action</th>
                                        </tr>
                                    </thead>
                                    @foreach ($data as $key => $user)
                                    <tbody>
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if(!empty($user->getRoleNames()))
                                                @foreach($user->getRoleNames() as $v)
                                                @if ($v == 'Administrator')
                                                <label class="badge badge-success">{{ $v }}</label>
                                                @else
                                                <label class="badge bg-gray">{{ $v }}</label>
                                                @endif
                                                @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                                                @can('user-edit')
                                                <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                                                @endcan
                                                {!! Form::open(['method' => 'DELETE','url' => ['users/delete',
                                                $user->id],'style'=>'display:inline'])!!}
                                                @can('user-delete')
                                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                @endcan
                                                {!! Form::close() !!}

                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                                {{$data->render()}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
