@extends('administrator::dashboard')
@section('user-show')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Show User</h1>
                <small>&nbsp; Show detail User </small>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('users')}}">Home</a></li>
                    <li class="breadcrumb-item active">Show User</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->





<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 col-md-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <div class="pull-left">
                            <h3>Show detail User</h3>
                        </div>
                    </div>

                    <div class="modal-body">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group text-center">
                                <strong>Name:</strong>
                                {{ $user->name }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group text-center">
                                <strong>Email:</strong>
                                {{ $user->email }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group text-center">
                                <strong>Roles:</strong>
                                @if(!empty($user->getRoleNames()))
                                @foreach($user->getRoleNames() as $v)
                                <label class="badge badge-success">{{ $v }}</label>
                                @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="text-center">
                            <a class="btn btn-secondary" href="{{ route('users') }}"><i class="fas fa-undo mr-2"></i> Back</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
