@extends('administrator::dashboard')
@section('role-show')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Show Role</h1>
                <small>&nbsp; Show detail Role </small>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('roles')}}">Home</a></li>
                    <li class="breadcrumb-item active">Show Role</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->


<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="mx-5">
                            <h3 class="text-center">Show detail Role</h3>
                        </div>
                    </div>
                    <div class="modal-body text-center mx-5">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Name:</strong>
                                {{ $role->name }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <h5>Permissions:</h5>
                                @if(!empty($rolePermissions))
                                @foreach($rolePermissions as $v)
                                <label class="label label-success">{{ $v->name }},</label><br>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        <a class="btn btn-secondary my-3" href="{{ route('roles') }}"><i class="fas fa-undo mr-2"></i> Back</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


@endsection
