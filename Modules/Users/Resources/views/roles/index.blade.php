@extends('administrator::dashboard')
@section('user-index')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Roles Management</h1>
                <small>&nbsp; List of roles with its permissions</small>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('roles')}}">Home</a></li>
                    <li class="breadcrumb-item active">Create Role</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->


<section>
    @if ($message = Session::get('success'))
    <div class="alert alert-success mx-5 alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p>{{ $message }}</p>
    </div>
    @endif
</section>



<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex">
                        <div class="pull-left">
                            <h3>List of Roles</h3>
                        </div>

                        {{-- Button Create Roles --}}
                        <div class="ml-auto">
                            @can('role-create')
                            <a class="btn btn-outline-success" href="{{route('roles.create')}}">
                                <i class="fas fa-user-edit mr-2"></i> Create New Role</a>
                            @endcan
                        </div>
                    </div>


                    <div class="card-body">
                        <div class="table-responsive">
                            {{-- Responsive Table --}}

                            {{-- Responsive Table --}}
                            <div class="box-body table-responsive">
                                <table id="example1" class="example1 table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Roles Name</th>
                                            <th width="280px">Action</th>
                                        </tr>
                                    </thead>
                                    @foreach ($roles as $key => $role)
                                    <tbody>
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $role->name }}</td>
                                            <td>
                                                <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
                                                @can('role-edit')
                                                <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                                                @endcan
                                                @can('role-delete')
                                                {!! Form::open(['method' => 'DELETE','url' => ['roles/delete',
                                                $role->id],'style'=>'display:inline']) !!}
                                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                                {{$roles->render()}}
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
