<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Users\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Illuminate\Support\Facades\Auth;
use Modules\Administrator\Classes\ReadModuleClass;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */


    /* Define roles users */
    public $roles;

    /* Read Modules */
    public $modules;


    function __construct()
    {
        /* Access permision for roles */
        $this->middleware('permission:role-list')->only(['index']);
        $this->middleware('permission:role-create')->only(['create']);
        $this->middleware('permission:role-edit')->only(['edit']);
        $this->middleware('permission:role-delete')->only(['destroy']);
        $this->middleware('auth');

        $this->roles = Auth::user();
        $this->modules = ReadModuleClass::read();
    }

    // Route to Roles management page
    public function index(Request $request)
    {
        $roles = Role::orderBy('id', 'DESC')->paginate(5);

        view()->share('module', $this->modules);

        return view('users::roles.index', compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    // Route to create roles page
    public function create()
    {
        $permission = Permission::get();

        view()->share('module', $this->modules);

        return view('users::roles.create', compact('permission'));
    }



    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    // Store to table permission
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);


        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));


        return redirect()->route('roles')
            ->with('success', 'Role created successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    // Show detail with permission list  of roles
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=", "permissions.id")
            ->where("role_has_permissions.role_id", $id)
            ->get();

        view()->share('module', $this->modules);

        return view('users::roles.show', compact('role', 'rolePermissions'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    // Redirect to edit page roles with getting data id roles
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();

        view()->share('module', $this->modules);


        return view('users::roles.edit', compact('role', 'permission', 'rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    // Update roles according id from edit function
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);


        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();


        $role->syncPermissions($request->input('permission'));


        return redirect()->route('roles')
            ->with('success', 'Role updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    // Delete Roles
    public function destroy($id)
    {
        //
        DB::table("roles")->where('id', $id)->delete();
        return redirect()->route('roles')
            ->with('success', 'Role deleted successfully');
    }
}
