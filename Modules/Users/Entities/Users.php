<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;

// Models to get data from database of users table
class Users extends Model
{
    protected $fillable = ['id', 'name', 'email'];
}
