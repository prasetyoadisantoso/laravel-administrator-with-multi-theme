@extends('administrator::dashboard')
@section('administrator-index')


<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Welcome !</h1>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('administrator')}}">Home</a></li>
                    <li class="breadcrumb-item active">Administrator Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->




<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">




                <div class="col-sm-12 col-xs-12 col-lg-12">
                    <div class="jumbotron bg-success disabled">
                        <h2 class="display-5">Welcome to Icicle Administrator Panel </h1>
                        <p class="lead"> This Panel Powered by <a href="http://laravel.com" class="text-white"
                                style="font-weight: bold;">Laravel</a></p>
                    </div>
                </div>

                <!-- Small boxes (Stat box) -->
                <div class="row">


                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{$total_module}}</h3>
                                <p>Total Modules Installed</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-cubes"></i>
                            </div>
                            <a href="#" class="small-box-footer">Including Default Modules</a>
                        </div>
                    </div>
                    <!-- ./col -->





                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{$total_user}}</h3>

                                <p>User Registrations</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="{{route('users')}}" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->




                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-teal">
                            <div class="inner">
                                <h3>{{$total_role}}</sup></h3>

                                <p>Total Roles with Permissions</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-unlock-alt"></i>
                            </div>
                            <a href="{{route('roles')}}" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->





                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>Sample</h3>
                                <p>You can add widget here</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="#" class="small-box-footer">Sample text</a>
                        </div>
                    </div>
                    <!-- ./col -->


                </div>
            </div>
        </div>
</section>






@endsection
