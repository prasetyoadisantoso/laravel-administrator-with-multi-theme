<?php

namespace Modules\Administrator\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Renderable;
use Modules\Administrator\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Administrator\Classes\ReadModuleClass;
use Modules\Administrator\Classes\UserClass;


class AdministratorController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    // roles permissions function
    public $roles;
    public $total_roles;

    // Read Modules
    public $modules;
    public $total_modules;

    // Read Users
    public $total_users;

    public function __construct()
    {
        $this->middleware('permission:administrator-access')->only(['index']);
        $this->middleware(['auth','verified']);
        $this->roles = Auth::user();
        $this->modules = ReadModuleClass::read();
        $this->total_modules = ReadModuleClass::total();
        $this->total_users = UserClass::getTotalUsers();
        $this->total_roles = UserClass::getTotalRoles();
    }



    /**
     * Dashboard Page
     */
    public function index()
    {

        /* Redirect Member to Frontend */
        if (Auth::user()->hasRole('Member')) {
            return redirect('/');
        }


        /* Share Module, Total Module, Total User, & Total Role into to Dashboard */
        view()->share('module', $this->modules);
        view()->share('total_module', $this->total_modules);
        view()->share('total_user', $this->total_users);
        view()->share('total_role', $this->total_roles);

        /* Go to Dashboard */
        return view('administrator::dashboard.administrator');
    }




    /**
     * Logout Function
     */
    public function logout()
    {
        /* Redirect to Front Page */
        Auth::logout();
        return redirect('/');
    }
}
