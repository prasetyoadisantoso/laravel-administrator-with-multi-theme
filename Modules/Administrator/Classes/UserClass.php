<?php
namespace Modules\Administrator\Classes;

use App\User;
use Spatie\Permission\Models\Role;

class UserClass {

    // show total users active on dashboard admin
    public static function getTotalUsers()
    {
        # code...
        $total = User::count();
        return $total;
    }

    public static function getTotalRoles()
    {
        # code...
        $total = Role::count();
        return $total;
    }

}
