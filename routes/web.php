<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route to Home Page
Route::get('/', 'MainController@index');







/**
 *
 * Routing for Clean Blog Function
 *
 * return post and postcat
 */

// Route to Posts Page
Route::get('posting', 'MainController@posting')->name('posting');

// Route to Categories Page
Route::get('post', 'MainController@postcat')->name('post.cat');

// Route to Portfolio Page
Route::get('portfolios', 'MainController@portfolio')->name('portfolios');
