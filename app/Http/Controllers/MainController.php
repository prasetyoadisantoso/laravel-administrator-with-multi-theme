<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Settings\Entities\Settings;
use App\User;
use Modules\CMS\Entities\Categories;
use Modules\CMS\Entities\Posts;

//Redirect to Blade at resources/themes
class MainController extends Controller
{

    public function index()
    {

        $user = auth()->user();

        // Get Themes from database
        $themes_now =  Settings::first();
        $themes = ($themes_now->themes);
        return view($themes . '.index', compact('user'));
    }



     /* Post Function */
    public function posting(Request $request)
    {

        /* Get Post from request */
        $array = $request->all();
        $posts = array_key_first($array);

        /* Remove underscore */
        $posts = str_replace('_', ' ', $posts);

        /* Get Post with same Title */
        $post = Posts::where('title', '=', $posts)->get();


        $user = auth()->user();

        $themes_now =  Settings::first();
        $themes = ($themes_now->themes);
        return view($themes . '.post', compact('user', 'post'));
    }


    /* Categories Function*/
    public function postcat(Request $request)
    {
        /* Get Category from request */
        $array = $request->all();
        $category = array_key_first($array);

        /* Remove underscore */
        $category = str_replace('_', ' ', $category);

        /* Get categories with same category */
        $data = Categories::with('posts')->where('category', '=', $category)->get();

        foreach ($data as $key => $value) {
            $posts = $value->posts;
        }




        /* Authentication */
        $user = auth()->user();

        /* Theme  */
        $themes_now =  Settings::first();
        $themes = ($themes_now->themes);

        return view($themes . '.categories', compact('user', 'posts', 'category'));
    }

    public function portfolio(Request $request)
    {
        $user = auth()->user();
        // Get Themes from database
        $themes_now =  Settings::first();
        $themes = ($themes_now->themes);
        return view($themes . '.portfolio', compact('user'));
    }
}
