<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Pagination\Paginator;
use Illuminate\Auth\Events\Authenticated;
use Modules\Administrator\Classes\ReadModuleClass;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Relation::morphMap([
            'App\User' => 'Modules\Users\Entities\User',
        ]);

        Paginator::useBootstrap();

        Schema::defaultStringLength(191);

        // Get User Active
        $this->app['events']->listen(Authenticated::class, function ($e) {
            view()->share('user', $e->user);
        });

    }
}
