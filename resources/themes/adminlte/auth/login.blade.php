@extends('adminlte.layouts.auth')

@section('content-login')
<div class="login-box">
    {{-- Login Logo --}}
    <div class="login-logo">
        <a href=""><b>ICICLE</b>LOGIN</a>
    </div>
    <!-- /.login-logo -->

    {{-- Login Form --}}
    <div class="card">
        <div class="card-body login-card-body pb-0 rounded shadow-lg">
            <p class="login-box-msg">Sign in to start your session</p>

            <form method="POST" action="{{ route('login') }}">

                @csrf

                <div class="input-group mb-3">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
                        placeholder="Email...">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong style="color: red;">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                        name="password" required autocomplete="current-password" placeholder="Password...">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-7 mt-2">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-5">
                        <button type="submit" class="btn btn-success btn-block">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>


            <div class="container">
                @if (Route::has('password.request'))
                <a class="btn btn-link text-dark" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif

                <a class="btn btn-link text-dark" href="{{ route('register') }}">
                    {{ __('Register') }}
                </a>
            </div>

        </div>
        <!-- /.login-card-body -->
    </div>

    <br>

    <div class="mx-3 text-center">
        <span>Created with ❤ by </span><a href="https://facebook.com/pras.element" class="text-dark">Prasetyo Adi
            Santoso</a>
        <br>
        <a href="{{url('/')}}" class="btn bg-olive mt-2">Go to Homepage</a>
    </div>
</div>

@endsection
