@extends('adminlte.layouts.auth')

@section('content-verify')

<div class="register-box">


    <div class="register-logo">
        <h3>{{ __('Verify Your Email Address') }}</h3>
    </div>

    <div class="card">
        <div class="card-body register-card-body">

            <div class="body">
                @if (session('resent'))
                <div class="alert alert-success" role="alert">
                    {{ __('A fresh verification link has been sent to your email address.') }}
                </div>
                @endif

                {{ __('Before proceeding, please check your email for a verification link.') }}
                {{ __('If you did not receive the email') }},


                <form class="d-inline text-center" method="POST" action="{{ route('verification.resend') }}">
                    @csrf
                    <br>
                    <button type="submit"
                        class="btn btn-secondary mt-2 mx-5">{{ __('click here to request another') }}</button>
                </form>
                <br>
            </div>

        </div>
    </div>

</div>
@endsection
