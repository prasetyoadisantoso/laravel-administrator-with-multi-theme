<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insert list of permissions here
        $permissions = [
            'administrator-access',
            'setting-access',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete'
         ];


         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }

    }
}
