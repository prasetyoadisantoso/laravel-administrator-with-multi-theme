<p align="center"><img src="https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/raw/master/Icicle%20Admin%20Panel.png" width="150"></p>

<p align="center">
<a href="https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/releases/stable-8.0.0"><img src="https://img.shields.io/badge/version-8.0.0-success" alt="Version"></a>
<a href="#"><img src="https://img.shields.io/badge/releases-stable-success" alt="Version"></a>
<a href="https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme/-/blob/master/LICENSE"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
<a href="#"><img src="https://img.shields.io/badge/php-8.0-blue" alt="Php Version"></a>

</p>


# ICICLE Administrator Panel

Icicle Administrator Panel made from Laravel 8 as a Solid Framework. Some Package used to build this Project.

### Frontend
- [AdminLTE](https://adminlte.io/).
- [Freelancer](https://startbootstrap.com/themes/freelancer/) as default theme.
- [Clean Blog](https://startbootstrap.com/themes/clean-blog/).

### Backend
- [Laravel Debug Bar](https://github.com/barryvdh/laravel-debugbar).
- [Laravel Collective](https://laravelcollective.com/).
- [Spatie Laravel Permission](https://spatie.be/open-source).
- [Nwidart Module](https://nwidart.com/laravel-modules/v6/introduction).

## Features
Features always update at the moment, and will added continuously without notification

### Administration Page
- Multi-Authentication and Roles Permissions
- Basic CRUD at Users Management System
- Basic CRUD at Role Management System
- Cache Management System
- Theme Management System

### Additional Module
- [CMS Module](https://gitlab.com/prasetyo.element/icicle-cms-module)
- [Portfolio Module](https://gitlab.com/prasetyo.element/icicle-portfolio-module)

### Client Page
- You can download some themes at [Icicle Front End](https://gitlab.com/prasetyo.element/icicle-front-end-development)

### Screenshot
![Screenshot_from_2020-11-12_23-47-01](/uploads/73d9253d9e466bd9af3d89fd2a64d2a3/Screenshot_from_2020-11-12_23-47-01.png)

## Installation
Support PHP 7.4, below 7.4 is not suggested for stability usage.

1. Clone this Project, Prepare Database & Setting .env 
2. `composer update`
3. `php artisan key:generate`
4. `php artisan migrate:reset` <- optional
5. `php artisan migrate:fresh --seed`
6. `php artisan serve` <- Run Laravel 

Username (Admin) : admin@icicle.io <br />
Username (Member) : member@icicle.io <br />
Password : 123456 <br />

**Note** : Activated all permissions to gain all control system at administration dashboard panel

### Setting Email with SMTP Gmail for Registration Verification
1. Activated [Google App Password](https://myaccount.google.com/apppasswords).
2. Create your app password and copy the password has been given.
3. If you want to set email with gmail account you can edit this code to your .env
<br>`MAIL_HOST=smtp.google.com`
<br>`MAIL_PORT=587`
<br>`MAIL_USERNAME=youremail@gmail.com`
<br>`MAIL_PASSWORD=yourapppassword`
<br>`MAIL_ENCRYPTION=tls` 

If you want to try this demo, [Click Here!](https://icicle-project.prasetyoadisantoso.com/).

## License

ICICLE Administrator Panel is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
